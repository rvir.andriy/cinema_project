package com.eleks.playhttp

import java.awt.print.Book

import javax.inject.Inject
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.{InjectedController, _}

class Controller @Inject() extends InjectedController {

  implicit val movieFormat = Json.format[Movie]
  implicit val scheduleFormat = Json.format[Schedule]

  def getAllMovies() = Action {
    val movies: Set[Schedule] = Service.getAllMovies()
    movies.size match {
      case 0 => NotFound(s"List of movies is empty!")
      case _ => Ok(Json.obj("Schedule" -> movies))
    }
  }

  def getById(idMovie: Long) = Action {
    val movie: Option[Movie] = Service.getById(idMovie)
    movie match {
      case Some(movie) => Ok(Json.toJson(movie))
      case None => NotFound(s"Movie with id = $idMovie does not exist")
    }
  }

  def getByGenre(Genre: String) = Action {
    val schedule: Set[Schedule] = Service.getScheduleByIdGenre(Genre)
    schedule.size match {
      case 0 => NotFound(s"List Films in Schedule with genre = $Genre is empty!")
      case _ => Ok(Json.obj("schedule" -> schedule))
    }
  }

  def deleteById(idMovie: Long) = Action {
    val delete: Option[Boolean] = Service.deleteById(idMovie)
    delete match {
      case Some(true) => Ok(s"Film with id = $idMovie is deleted")
      case Some(false) => Ok(s"Film with id = $idMovie is not deleted. There exists schedule!")
      case None => NotFound(s"Film with id = $idMovie does not exist")
    }
  }

  def addSchedule(): Action[JsValue] = Action(parse.json) { request =>
    val schedule = request.body.as[Schedule]
    val result:Option[Boolean] = Service.addSchedule(schedule)

    result match {
      case Some(true) => Created("Schedule was created")
      case Some(false) => NotAcceptable(s"There exists similar schedule!")
      case None => NotFound(s"Schedule was not created!")
    }
}
}