package com.eleks.playhttp

import java.sql.DriverManager
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Statement
import java.sql.Driver
import java.text.SimpleDateFormat
import java.util.Date

object Database {
  var allSchedule: Set[Schedule] =_
  val dateFormat = new SimpleDateFormat("EEE, MMM dd, yyyy h:mm a")
  var conn:Connection =_

  var movies: Set[Movie] = Set(
    Movie(1, "Movie1", "Action"),
    Movie(2, "Movie2", "Comedy"),
    Movie(3, "Movie3", "Adventure"),
    Movie(4, "Movie4", "Horror"),
    Movie(5, "Movie5", "Adventure"),
    Movie(6, "Movie6", "Action")
  )

  var schedule: Set[Schedule] = Set(
    Schedule(1, "25.05.2018:11.30", "Movie1", "Action", 50.00, 1),
    Schedule(2, "25.05.2018:13.30", "Movie2", "Comedy", 50.00, 1),
    Schedule(3, "25.05.2018:15.30", "Movie3", "Adventure", 50.00, 1),
    Schedule(4, "25.05.2018:17.30", "Movie4", "Horror", 50.00, 1),
    Schedule(5, "25.05.2018:20.30", "Movie5", "Adventure", 50.00, 1),
    Schedule(6, "25.05.2018:22.30", "Movie6", "Action", 50.00, 1),
    Schedule(7, "25.05.2018:20.30", "Movie1", "Action", 50.00, 2),
    Schedule(8, "25.05.2018:18.30", "Movie2", "Comedy", 50.00, 2),
    Schedule(9, "25.05.2018:16.30", "Movie3", "Adventure", 50.00, 2),
    Schedule(10, "25.05.2018:14.30", "Movie4", "Horror", 50.00, 2),
    Schedule(11, "25.05.2018:12.30", "Movie5", "Adventure", 50.00, 2),
    Schedule(12, "25.05.2018:10.30", "Movie6", "Action", 50.00, 2)
  )
//**********************************************************************
  def getAllMovies(): Set[Schedule]={
    try{
      getConnection()
    }catch {
      case e:Throwable => e.printStackTrace()
    }
//    CREATE VIEW  AllMovies  AS
//    SELECT        sc.id, sc.time, mv.name, mv.genre, sc.price, sc.hall_id
//    FROM            dbo.Schedule AS sc INNER JOIN
//    dbo.Movie AS mv ON sc.movie_id = mv.id
    val sql = "select id, time, name, genre, price, hall_id from AllMovies"
    val statement = conn.createStatement()
    val rs = statement.executeQuery(sql)
    val builder = Set.newBuilder[Schedule]
    while (rs.next()) {

      builder+=Schedule(rs.getInt("id"),dateFormat.format(rs.getDate("time")),rs.getString("name").trim() ,rs.getString("genre").trim(),rs.getFloat("price"),rs.getInt("hall_id"))
    }
    return builder.result()
  }
//***********************************************************************
  def getById(idMovie: Long): Option[Movie] = {
    try{
      getConnection()
    }catch {
      case e:Throwable => e.printStackTrace()
    }
    println("getConnection")
    val sql = "SELECT id, name, genre FROM Movie WHERE id=%s".format(idMovie)
    val statement = conn.createStatement()
    println("statement"+sql)
    val rs = statement.executeQuery(sql)
    println("executeQuery")
    if(rs.next()){
      val res  = Movie(rs.getInt("id"),rs.getString("name").trim(),rs.getString("genre").trim())
      return Some(res)
    }else{
      return None
    }
  }
//******************************************************************
  def getScheduleByIdGenre(Genre: String): Set[Schedule] = {
    try{
      getConnection()
    }catch {
      case e:Throwable => e.printStackTrace()
    }
    val sql = "select id, time, name, genre, price, hall_id from AllMovies where genre='%s'".format(Genre)
    val statement = conn.createStatement()
    val rs = statement.executeQuery(sql)
    val builder = Set.newBuilder[Schedule]
    while (rs.next()) {
      builder+=Schedule(rs.getInt("id"),dateFormat.format(rs.getDate("time")),rs.getString("name").trim(),rs.getString("genre").trim(),rs.getFloat("price"),rs.getInt("hall_id"))
    }
    return builder.result()

    schedule.filter(_.genre == Genre)
  }
  //******************************************************************

  def deleteById(idMovie: Long): Option[Boolean] = {
    try{
      getConnection()
    }catch {
      case e:Throwable => e.printStackTrace()
    }

    val check: Option[Movie] =getById(idMovie)
    check match {
        case None => return None
        case Some(check)=>println("cheked")
    }

    var sql = "select * from Schedule where movie_id=%s".format(idMovie)
    val statement = conn.createStatement()
    val rs = statement.executeQuery(sql)
    val builder = Set.newBuilder[Schedule]
    if(rs.next()){
      println("There exist film in schedule!")
      return Some(false)
    }

    sql = "DELETE FROM Movie WHERE id=%s".format(idMovie)
    //val statement = conn.createStatement()
    if(statement.executeUpdate(sql)==1){
      return Some(true)
    }else{
      return Some(false)
    }
  }
  //******************************************************************

  def addSchedule(newschedule: Schedule): Option[Boolean] = {
    try{
      getConnection()
    }catch {
      case e:Throwable => e.printStackTrace()
    }
    var statement = conn.createStatement()
    try {
      // Check Movie
      var sql = "select id from Movie where name = '%s' and genre='%s'".format(newschedule.movie, newschedule.genre)
      var rs = statement.executeQuery(sql)
      var idMovie = 0
      if (rs.next()) {
        //get id Movie
        idMovie = rs.getInt("id")
      } else { //Create new Movie
        sql = "select max(id) as idMax from Movie"
        rs = statement.executeQuery(sql)
        rs.next()
        idMovie = rs.getInt("idMax") + 1
        sql = "insert into Movie values (%s,'%s','%s')".format(idMovie, newschedule.movie, newschedule.genre)
        if (statement.executeUpdate(sql) == 0){
          println("cant create new Movie")
          return None
        }
      }
      // Check Schedule
      sql = "select * from Schedule where movie_id=%s or (time='%s' and hall_id=%s)".format(newschedule.id, newschedule.time, newschedule.hall_id)
      rs = statement.executeQuery(sql)
      if (rs.next()) { //exixsts Schedule?
        return Some(false)
      } else { // Create new Schedule
        sql = "insert into  Schedule (id, time, movie_id, price, hall_id) values (%s,'%s',%s, %s, %s)".format(newschedule.id, newschedule.time, idMovie, newschedule.price, newschedule.hall_id)
        if (statement.executeUpdate(sql) == 0) {
          println("cant create new Scedule")
          return None
        } else {
          return Some(true)
        }
      }
    }catch {
      case e:Throwable => {
        println("addSchedule -> Something wrong!!!\n\n"+e.printStackTrace())
        return None

      }
    }finally {
      conn.close()
    }
  }

  //**********************************************
  def getConnection() = {

    val driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver"
    var url = "jdbc:sqlserver://DESKTOP-PP08V8M\\ELEKSDATACAMP1;databaseName=Cinema;integratedSecurity=true"

    try {
      Class.forName(driver)
      conn = DriverManager.getConnection(url)
    } catch {
      case e:Throwable => e.printStackTrace()
    }
  }

}
