package com.eleks.playhttp

case class Movie(id: Int, name: String, genre: String)
