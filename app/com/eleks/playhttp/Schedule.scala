package com.eleks.playhttp

case class Schedule (id: Int, time: String, movie: String, genre: String, price: Double, hall_id: Int)
