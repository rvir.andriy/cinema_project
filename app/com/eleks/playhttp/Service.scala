package com.eleks.playhttp

object Service {

  def getAllMovies(): Set[Schedule] = Database.getAllMovies()

  def getById(idMovie: Long):Option[Movie]=Database.getById(idMovie)

  def getScheduleByIdGenre(Genre: String):Set[Schedule]=Database.getScheduleByIdGenre(Genre)

  def deleteById(idMovie: Long):Option[Boolean]=Database.deleteById(idMovie)

  def addSchedule(schedule: Schedule): Option[Boolean] = Database.addSchedule(schedule)

}
